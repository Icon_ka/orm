package main

type YourStructName1 struct {
	Field1 string `db:"field1"`
	Field2 int    `db:"field2"`
}

func (s *YourStructName1) Method1() {
	// Some logic here
}

func (s *YourStructName1) Method2() {
	// Some logic here
}

type YourStructName2 struct {
	Field3 float64
}

func (s *YourStructName2) Method3() {
	// Some logic here
}

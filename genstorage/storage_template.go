package genstorage

const StorageTemplate = `package repository


import (
	"context"
	"fmt"
	"gitlab.com/golight/dao"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/dao/types"

	"time"
)

type {{ .EntityNameUppercase }}Storage struct {
    adapter dao.DAOFace
}

func New{{ .EntityNameUppercase}}Storage(sqlAdapter dao.DAOFace) {{ .EntityNameUppercase }}Storage {
    return {{ .EntityNameUppercase }}Storage{adapter: sqlAdapter}
}

func ({{ .EntityFirstLetter }} *{{ .EntityNameUppercase }}Storage) Create(ctx context.Context, dto {{ .PackageName }}.{{ .EntityName }}) (int64, error) {
    return {{ .EntityFirstLetter }}.adapter.Create(ctx, &dto)
}

func ({{ .EntityFirstLetter }} *{{ .EntityNameUppercase }}Storage) Update(ctx context.Context, dto {{ .PackageName }}.{{ .EntityName }}) error {
	return {{ .EntityFirstLetter }}.adapter.Update(
		ctx,
		&dto,
		params.Condition{
			Equal: map[string]interface{}{"id": dto.ID},
		},
		params.Update,
	)
}

func ({{ .EntityFirstLetter }} *{{ .EntityNameUppercase }}Storage) GetByID(ctx context.Context, {{ .EntityNameLowercase }}ID int) ({{ .PackageName }}.{{ .EntityName }}, error) {
	var list []{{ .PackageName }}.{{ .EntityName }}
	var table {{ .PackageName }}.{{ .EntityName }}
	err := {{ .EntityFirstLetter }}.adapter.List(ctx, &list, &table, params.Condition{
		Equal: map[string]interface{}{"id": {{ .EntityNameLowercase }}ID},
	})
	if err != nil {
		return {{ .PackageName }}.{{ .EntityName }}{}, err
	}	
	if len(list) < 1 {
		return {{ .PackageName }}.{{ .EntityName }}{}, fmt.Errorf("{{ .EntityNameLowercase }} storage: GetByID not found")
	}
	return list[0], err
}

func ({{ .EntityFirstLetter }} *{{ .EntityNameUppercase }}Storage) GetList(ctx context.Context, condition params.Condition) ([]{{ .PackageName }}.{{ .EntityName }}, error) {
	var list []{{ .PackageName }}.{{ .EntityName }}
	var table {{ .PackageName }}.{{ .EntityName }}
	err := {{ .EntityFirstLetter }}.adapter.List(ctx, &list, &table, condition)
	if err != nil {
		return nil, err
	}

	return list, nil
}

func ({{ .EntityFirstLetter }} *{{ .EntityNameUppercase }}Storage) Delete(ctx context.Context, {{ .EntityNameLowercase }}ID int) error {
	table, err := {{ .EntityFirstLetter }}.GetByID(ctx, {{ .EntityNameLowercase }}ID)
	if err != nil {
		return err
	}

	table.DeletedAt = types.NewNullTime(time.Now())

	return {{ .EntityFirstLetter }}.adapter.Update(
		ctx,
		&table,
		params.Condition{
			Equal: map[string]interface{}{"id": table.ID},
		},
		params.Update,
	)
}`

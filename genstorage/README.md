# Genstorage

The Genstorage package is designed to automatically generate storage and interface files

## Installation

```
go get "gitlab.com/golight/orm"
```

## Description

```go
//getting file name from cmd
func GetFileName() (string, error)
```
```go
//getting table name in NewStorage function
func GetTableName(fileName string) (string, error)
```
```go
//getting structure name in NewStorage function
func GetStructName(fileName string) (string, error)
```
```go
//getting structure name in NewStorage function
func GetPackageName(fileName string) (string, error)
```
```go
//creating new storage
func NewStorage(filename string) (*Storage, error)
```
```go
//creating new interface template
func NewInterfaceTemplate() (*template.Template, error)
```
```go
//creating new files in storage directory
func (s *Storage) CreateStorageFiles() error
```
```go
//creating new storage template
func NewStorageTemplate() (*template.Template, error)
```
## Usage

```go
func main() {
	file, err := GetFileName()
	if err != nil {
		return
}
	storage, err := NewStorage(file)
	if err != nil {
		return
}
	err = storage.CreateStorageFiles()
	if err != nil {
		return
}
}
```

## Roadmap
- При переносе genstorage в отдельный пакет необходимо проверить импорты в использующих genstorage пакетах
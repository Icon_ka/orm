package genstorage

const InterfaceTemplate = `package repository

import (
	"context"
	"gitlab.com/golight/dao/params"
)

type {{ .EntityNameUppercase }}er interface {
	Create(ctx context.Context, dto {{ .PackageName }}.{{ .EntityName }}) (int64, error)
	Update(ctx context.Context, dto {{ .PackageName }}.{{ .EntityName }}) error
	GetByID(ctx context.Context, {{ .EntityNameLowercase }}ID int) ({{ .PackageName }}.{{ .EntityName }}, error)
	GetList(ctx context.Context, condition params.Condition) ([]{{ .PackageName }}.{{ .EntityName }}, error)
	Delete(ctx context.Context, {{ .EntityNameLowercase }}ID int) error
}`
